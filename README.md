# Bill Discount

This program calculates the net amount to pay after a discount based on a given bill, which contains the user, items and each item quantity.

Notes to consider before starting:
```
- Items, users, item types, user account types and bills are already created under an H2 embedded memory database
- Data will be inserted when the application starts using the file "data.sql" under /src/main/resources/
- You can modify the bill payments query to add different scenarios
- Different discount types are covered
- You can get the result either by running the Tests or by using REST webservice (POSTMAN etc..). URL: localhost:8080/SpringBootDemo/returnDiscountResult it will be a POST method that takes the Bill Id (BILL_PAYMENT_ID from BILL_PAYMENT table) as parameter and returns a JSON response containing the Error Code, Error Description and the Total Net Amount after the discount
- The project was made simple containing one method in the controller, following MVC standard (controller, service, repository)
- To check all the existing data on database, go to IP:PORT(localhost:8080)/h2/, choose "Generic H2 (Embedded)", driver class "org.h2.Driver", JDBC URL "jdbc:h2:mem:testdb", Username "sa", no password and connect
```

## DB Diagram
![alt text](https://bitbucket.org/euriskoteam/retail-website-java-backend-eddy/raw/bd1bc76be4c75028c912afb4d7be0fdfcc2226c3/src/main/resources/Diagram/Diagram.png)
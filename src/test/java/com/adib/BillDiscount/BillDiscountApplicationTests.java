package com.adib.BillDiscount;

import com.adib.BillDiscount.billdiscountservice.BillDiscountService;
import com.adib.BillDiscount.datamodel.BillPayment;
import com.adib.BillDiscount.datamodel.FinalResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BillDiscountApplicationTests
{
    @Autowired
    BillDiscountService bill;

    @Test
    public void billDiscountTest()
    {
        try
        {
            FinalResponse finalResponse;

            String billId               = "1"; //changing this value for testing
            List<BillPayment> list      = bill.getUserBill(billId);

            if(list.size() > 0)
            {
                finalResponse = bill.getBillResult(list);

                System.out.println("Total Net to Pay [" + finalResponse.getTotalNetAmount() + "]");
            }
            else
            {
                finalResponse = new FinalResponse();
                finalResponse.setErrorCode("102");
                finalResponse.setErrorDescription("Bill " + billId + " was not found.");
            }

            System.out.println("Error Code [" + finalResponse.getErrorCode() + "]");
            System.out.println("Error Description [" + finalResponse.getErrorDescription() + "]");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
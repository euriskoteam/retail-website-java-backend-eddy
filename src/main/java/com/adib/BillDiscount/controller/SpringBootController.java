package com.adib.BillDiscount.controller;

import com.adib.BillDiscount.billdiscountservice.BillDiscountService;
import com.adib.BillDiscount.datamodel.BillPayment;
import com.adib.BillDiscount.datamodel.FinalResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/SpringBootDemo")
public class SpringBootController
{
    @Autowired
    BillDiscountService billDiscountService;

    @PostMapping(value = "/returnDiscountResult", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public FinalResponse BillDiscountController(@RequestBody String billId)
    {
        FinalResponse response = null;

        try
        {
            if(billDiscountService.isInteger(billId))
            {
                List<BillPayment> billPay = billDiscountService.getUserBill(billId);

                if(billPay.size() > 0)
                {
                    response = billDiscountService.getBillResult(billPay);
                }
                else
                {
                    response = new FinalResponse();
                    response.setErrorCode("102");
                    response.setErrorDescription("Bill '" + billId + "' was not found.");
                }
            }
            else
            {
                response = new FinalResponse();
                response.setErrorCode("103");
                response.setErrorDescription("The parameter " + billId + " is not a bill id.");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return response;
    }
}
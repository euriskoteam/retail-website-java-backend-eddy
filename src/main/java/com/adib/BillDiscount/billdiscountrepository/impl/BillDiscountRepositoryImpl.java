package com.adib.BillDiscount.billdiscountrepository.impl;

import com.adib.BillDiscount.billdiscountrepository.BillDiscountRepository;
import com.adib.BillDiscount.datamodel.BillPayment;
import com.adib.BillDiscount.datamodel.Item;
import com.adib.BillDiscount.datamodel.UserInfo;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class BillDiscountRepositoryImpl implements BillDiscountRepository
{
    @Autowired
    EntityManager entityManager;

    @Transactional
    public void persist(BillPayment billPayment)
    {
        entityManager.persist(billPayment);
    }

    @Transactional
    public Item getItemInfo(String itemId)
    {
        Session session = entityManager.unwrap(Session.class);
        Query query = session.createQuery("FROM Item WHERE itemId = :itemId");
        query.setParameter("itemId", Integer.parseInt(itemId));

        return (Item) query.getSingleResult();
    }

    @Transactional
    public UserInfo getUserInfo(String userId)
    {
        Session session = entityManager.unwrap(Session.class);
        Query query = session.createQuery("FROM UserInfo WHERE userId = :userId");
        query.setParameter("userId", Integer.parseInt(userId));

        return (UserInfo) query.getSingleResult();
    }

    @Transactional
    public List<BillPayment> getUserBill(String billId)
    {
        Session session = entityManager.unwrap(Session.class);
        Query query = session.createQuery("from BillPayment where billPaymentId = : billId");
        query.setParameter("billId", Integer.parseInt(billId));

        return query.list();
    }
}
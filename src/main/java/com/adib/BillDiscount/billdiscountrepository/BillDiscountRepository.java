package com.adib.BillDiscount.billdiscountrepository;

import com.adib.BillDiscount.datamodel.BillPayment;
import com.adib.BillDiscount.datamodel.Item;
import com.adib.BillDiscount.datamodel.UserInfo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BillDiscountRepository
{
    List<BillPayment> getUserBill(String billId);
    void persist(BillPayment billPayment);
    UserInfo getUserInfo(String userId);
    Item getItemInfo(String itemId);
}
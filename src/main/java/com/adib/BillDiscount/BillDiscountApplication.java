package com.adib.BillDiscount;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BillDiscountApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(BillDiscountApplication.class, args);
    }
}
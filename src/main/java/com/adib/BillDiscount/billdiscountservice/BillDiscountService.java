package com.adib.BillDiscount.billdiscountservice;

import com.adib.BillDiscount.datamodel.BillPayment;
import com.adib.BillDiscount.datamodel.FinalResponse;

import java.util.List;

public interface BillDiscountService
{
    FinalResponse getBillResult(List<BillPayment> billPayments) throws Exception;
    List<BillPayment> getUserBill(String billId) throws Exception;
    boolean isInteger(String parameter);
}
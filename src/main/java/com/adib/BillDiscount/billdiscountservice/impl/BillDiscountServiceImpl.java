package com.adib.BillDiscount.billdiscountservice.impl;

import com.adib.BillDiscount.billdiscountrepository.BillDiscountRepository;
import com.adib.BillDiscount.billdiscountservice.BillDiscountService;
import com.adib.BillDiscount.datamodel.BillPayment;
import com.adib.BillDiscount.datamodel.FinalResponse;
import com.adib.BillDiscount.datamodel.Item;
import com.adib.BillDiscount.datamodel.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
public class BillDiscountServiceImpl implements BillDiscountService
{
    @Autowired
    /* data access layer */
    BillDiscountRepository billDiscountRepository;

    public List<BillPayment> getUserBill(String billId)
    {
        return billDiscountRepository.getUserBill(billId);
    }

    /* calculating the discount and returning the discounted amount with its related description */
    public FinalResponse getBillResult(List<BillPayment> billPayments)
    {
        String itemPrice;
        String itemTypeId;
        String itemId;
        Item item;
        Date parsedBeginDate;
        String itemQuantity;
        BillPayment billPayment;
        FinalResponse finalResponse;

        long difference         = 0;
        double billTotalAmount  = 0.0;
        double groceryAmount    = 0.0;
        String userId           = billPayments.get(0).getUserInfo().getUserId() + ""; // get(0) since a bill_payment_id should only have one user
        UserInfo userInfo       = billDiscountRepository.getUserInfo(userId);
        String userRelationType = userInfo.getAccountTypes().getAccountTypeId() != null ? userInfo.getAccountTypes().getAccountTypeId() + "" : null;
        String accountBeginDate = userInfo.getAccountBeginDate();
        parsedBeginDate         = this.validateDate(accountBeginDate);

        /* calculating the difference in years between account date and system date */
        if(parsedBeginDate != null)
        {
            Date systemDate = new Date();
            difference      = (systemDate.getTime() - parsedBeginDate.getTime())/1000/60/60/24/30/12;
        }

        int listSize = billPayments.size();

        for(int i = 0; i < listSize; i++)
        {
            billPayment     = billPayments.get(i);
            itemId          = billPayment.getItem().getItemId() + "";
            item            = billDiscountRepository.getItemInfo(itemId);
            itemPrice       = item.getItemPrice() + "";
            itemQuantity    = billPayment.getItemQuantity() + "";

            if(!"".equals(itemPrice) && !"".equals(itemQuantity))
            {
                billTotalAmount = billTotalAmount + (Double.parseDouble(itemPrice) * Double.parseDouble(itemQuantity));
                itemTypeId      = item.getItemType() + "";

                if("2".equals(itemTypeId)) /* item is grocery */
                {
                    groceryAmount = groceryAmount + (Double.parseDouble(itemPrice) * Double.parseDouble(itemQuantity));
                }
            }
        }

        finalResponse           = new FinalResponse();
        HashMap discountMap     = this.calculateAmounts(billTotalAmount, groceryAmount, difference, userRelationType);
        String newTotalAmount   = discountMap.get("totalNetAmount") + "";

        finalResponse.setErrorCode(Double.parseDouble(newTotalAmount) != billTotalAmount ? "100" : "101");
        finalResponse.setTotalNetAmount(newTotalAmount);
        finalResponse.setErrorDescription(Double.parseDouble(newTotalAmount) != billTotalAmount ?
                "The following discounts were applied: " + discountMap.get("discountType") : "No discounts were applied.");

        return finalResponse;
    }

    /* calculating the discounts based on the total amount and the user account */
    private HashMap calculateAmounts(Double totalAmount, Double groceryAmount, long years, String userAccountType)
    {
        String discountType         = "";
        double amountToDiscount     = 0.0;
        HashMap response            = new HashMap();
        Double nonGroceryAmount     = totalAmount - groceryAmount;

        if ("1".equals(userAccountType)) /* the user is employee */
        {
            nonGroceryAmount    = nonGroceryAmount - nonGroceryAmount * 30 / 100;
            discountType        = discountType + ",30% discount on non-groceries as an employee";
        }
        else if ("2".equals(userAccountType)) /* the user is an affiliate */
        {
            nonGroceryAmount    = nonGroceryAmount - nonGroceryAmount * 10 / 100;
            discountType        = discountType + ",10% discount on non-groceries as an affiliate";
        }
        else if (years > 2 && "3".equals(userAccountType)) /* the user is a customer for more than 2 years */
        {
            nonGroceryAmount    = nonGroceryAmount - nonGroceryAmount * 5 / 100;
            discountType        = discountType + ",5% discount on non-groceries for your over 2 years loyalty";
        }

        if(totalAmount >= 100) /* getting the 5$ discount per 100$ */
        {
            amountToDiscount    = Math.floor((nonGroceryAmount + groceryAmount)/100) * 5;
            discountType        = discountType + ",5$ discount per 100$";
        }

        totalAmount = nonGroceryAmount + groceryAmount - amountToDiscount;

        response.put("totalNetAmount", totalAmount);
        response.put("discountType", discountType.replaceFirst(",", "") + ".");

        return response;
    }

    /* checking if the account date has the right date format */
    private Date validateDate(String dateStr)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        try
        {
            return dateFormat.parse(dateStr);
        }
        catch (Exception e)
        {
            return null;
        }
    }

    /* checking if the parameter is an integer or not */
    public boolean isInteger(String parameter)
    {
        boolean isInteger = true;

        try
        {
            Integer.parseInt(parameter);
        }
        catch(Exception e)
        {
            isInteger = false;
        }

        return isInteger;
    }
}
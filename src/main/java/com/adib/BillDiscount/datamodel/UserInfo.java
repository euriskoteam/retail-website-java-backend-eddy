package com.adib.BillDiscount.datamodel;

import javax.persistence.*;
import java.util.Set;

@Entity
public class UserInfo
{
    public UserInfo()
    {
        super();
    }

    public UserInfo(Integer userId, String userName, String accountBeginDate, AccountType accountTypes, Set<BillPayment> billPayments)
    {
        this.userId = userId;
        this.userName = userName;
        this.accountBeginDate = accountBeginDate;
        this.accountTypes = accountTypes;
        this.billPayments = billPayments;
    }

    @Id
    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "account_begin_date")
    private String accountBeginDate;

    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="accountTypeId")
    private AccountType accountTypes;

    @OneToMany(mappedBy = "userInfo")
    private Set<BillPayment> billPayments;

    public AccountType getAccountTypes()
    {
        return accountTypes;
    }

    public void setAccountTypes(AccountType accountTypes)
    {
        this.accountTypes = accountTypes;
    }

    public Set<BillPayment> getBillPayments()
    {
        return billPayments;
    }

    public void setBillPayments(Set<BillPayment> billPayments)
    {
        this.billPayments = billPayments;
    }

    public Integer getUserId()
    {
        return userId;
    }

    public void setUserId(Integer userId)
    {
        this.userId = userId;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getAccountBeginDate()
    {
        return accountBeginDate;
    }

    public void setAccountBeginDate(String accountBeginDate)
    {
        this.accountBeginDate = accountBeginDate;
    }
}
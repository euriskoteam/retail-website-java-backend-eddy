package com.adib.BillDiscount.datamodel;

import javax.persistence.*;
import java.util.Set;

@Entity
public class AccountType
{
    public AccountType()
    {
        super();
    }

    public AccountType(String accountTypeId, String accountTypeName, Set<UserInfo> userInfos)
    {
        this.accountTypeId = accountTypeId;
        this.accountTypeName = accountTypeName;
        this.userInfos = userInfos;
    }

    @Id
    @Column(name = "account_type_id")
    private String accountTypeId;

    @Column(name = "account_type_name")
    private String accountTypeName;

    @OneToMany(mappedBy = "accountTypes")
    private Set<UserInfo> userInfos;

    public Set<UserInfo> getUserInfos()
    {
        return userInfos;
    }

    public void setUserInfos(Set<UserInfo> userInfos)
    {
        this.userInfos = userInfos;
    }

    public String getAccountTypeId()
    {
        return accountTypeId;
    }

    public void setAccountTypeId(String accountTypeId)
    {
        this.accountTypeId = accountTypeId;
    }

    public String getAccountTypeName()
    {
        return accountTypeName;
    }

    public void setAccountTypeName(String accountTypeName)
    {
        this.accountTypeName = accountTypeName;
    }
}
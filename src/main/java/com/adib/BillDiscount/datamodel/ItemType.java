package com.adib.BillDiscount.datamodel;

import javax.persistence.*;
import java.util.Set;

@Entity
public class ItemType
{
    public ItemType()
    {
        super();
    }

    public ItemType(int itemTypeId, String itemTypeName)
    {
        this.itemTypeId = itemTypeId;
        this.itemTypeName = itemTypeName;
    }

    @Id
    @Column(name = "item_type_id")
    private int itemTypeId;

    @Column(name = "item_type_name")
    private String itemTypeName;

    @OneToMany(mappedBy = "itemType")
    private Set<Item> items;

    public Set<Item> getItems()
    {
        return items;
    }

    public void setItems(Set<Item> items)
    {
        this.items = items;
    }

    public int getItemTypeId()
    {
        return itemTypeId;
    }

    public void setItemTypeId(int itemTypeId)
    {
        this.itemTypeId = itemTypeId;
    }

    public String getItemTypeName()
    {
        return itemTypeName;
    }

    public void setItemTypeName(String itemTypeName)
    {
        this.itemTypeName = itemTypeName;
    }
}
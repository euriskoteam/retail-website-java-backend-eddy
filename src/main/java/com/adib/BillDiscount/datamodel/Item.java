package com.adib.BillDiscount.datamodel;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Item
{
    public Item()
    {
        super();
    }

    public Item(int itemId, String itemName, ItemType itemType, Set<BillPayment> billPayments, float itemPrice)
    {
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemType = itemType;
        this.billPayments = billPayments;
        this.itemPrice = itemPrice;
    }

    @Id
    @Column(name="item_id")
    private int itemId;

    @Column(name="item_name")
    private String itemName;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="itemTypeId")
    private ItemType itemType;

    @OneToMany(mappedBy = "item")
    private Set<BillPayment> billPayments;

    @Column(name="item_price")
    private float itemPrice;

    public Set<BillPayment> getBillPayments()
    {
        return billPayments;
    }

    public void setBillPayments(Set<BillPayment> billPayments)
    {
        this.billPayments = billPayments;
    }

    public ItemType getItemType()
    {
        return itemType;
    }

    public void setItemType(ItemType itemType)
    {
        this.itemType = itemType;
    }

    public int getItemId()
    {
        return itemId;
    }

    public void setItemId(int itemId)
    {
        this.itemId = itemId;
    }

    public String getItemName()
    {
        return itemName;
    }

    public void setItemName(String itemName)
    {
        this.itemName = itemName;
    }

    public float getItemPrice()
    {
        return itemPrice;
    }

    public void setItemPrice(float itemPrice)
    {
        this.itemPrice = itemPrice;
    }
}
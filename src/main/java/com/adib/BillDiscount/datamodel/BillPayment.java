package com.adib.BillDiscount.datamodel;

import javax.persistence.*;

@Entity
public class BillPayment
{
    public BillPayment()
    {
        super();
    }

    public BillPayment(int billPaymentSeq, int billPaymentId, UserInfo userInfo, Item item, int itemQuantity)
    {
        this.billPaymentSeq = billPaymentSeq;
        this.billPaymentId = billPaymentId;
        this.userInfo = userInfo;
        this.item = item;
        this.itemQuantity = itemQuantity;
    }

    @Id
    @Column(name="bill_payment_seq")
    private int billPaymentSeq;

    @Column(name="bill_payment_id")
    private int billPaymentId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="userId")
    private UserInfo userInfo;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="itemId")
    private Item item;

    @Column(name="item_quantity")
    private int itemQuantity;

    public UserInfo getUserInfo()
    {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo)
    {
        this.userInfo = userInfo;
    }

    public Item getItem()
    {
        return item;
    }

    public void setItem(Item item)
    {
        this.item = item;
    }

    public int getBillPaymentSeq()
    {
        return billPaymentSeq;
    }

    public void setBillPaymentSeq(int billPaymentSeq)
    {
        this.billPaymentSeq = billPaymentSeq;
    }

    public int getBillPaymentId()
    {
        return billPaymentId;
    }

    public void setBillPaymentId(int billPaymentId)
    {
        this.billPaymentId = billPaymentId;
    }

    public int getItemQuantity()
    {
        return itemQuantity;
    }

    public void setItemQuantity(int itemQuantity)
    {
        this.itemQuantity = itemQuantity;
    }
}